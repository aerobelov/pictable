//
//  ContentView.swift
//  pictable
//
//  Created by Pavel Belov on 17.10.2019.
//  Copyright © 2019 Pavel Belov. All rights reserved.
//

import SwiftUI

//Picture class with downloader
class Picture: Identifiable, ObservableObject {
    @Published var img: UIImage?
    var id = UUID()
    var urlstr = "https://placehold.it/375x150?text="
    init(num: Int) {
        guard let url = URL(string: urlstr + String(num)) else { return }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, let img = UIImage(data: data) else { return }
            DispatchQueue.main.async {
                self.img = img
            }
        }
        task.resume()
    }
}

//Array of pictures
var pics: [Picture]  {
    var temp: [Picture] = []
    for i in 1...100 {
        temp.append(Picture(num:i))
    }
    return temp
}

//Pictures row in SwiftUI list
struct picsRow:  View {
    @ObservedObject var pc: Picture
    var body: some View {
        HStack {
            Image(uiImage: pc.img != nil ? pc.img! : UIImage())
            .aspectRatio(contentMode: .fill)
        }
    }
}

struct ContentView: View {
    var body: some View {
        List(pics) { pic in picsRow(pc: pic)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
